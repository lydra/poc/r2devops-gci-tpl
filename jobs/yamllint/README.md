## Objective

Lint Yaml files with [yamllint](https://gitlab.com/pipeline-components/yamllint).

## Variables

| Name | Description | Default |
| ---- | ----------- | ------- |
| `files_to_lint` | liste of files to lint | ` ` |
| `working_directory` | Path of working directory | `$CI_PROJECT_DIR` |
